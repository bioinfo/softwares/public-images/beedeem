# BeeDeeM

This is the project to auto build the [BeeDeeM](https://github.com/pgdurand/BeeDeeM/tree/master) Docker image.

Docker recipe can be found [here](https://github.com/pgdurand/BeeDeeM/blob/master/docker/Dockerfile).

[How to pull and use BeeDeeM pre-built image](https://github.com/pgdurand/BeeDeeM/blob/master/docker/README.md#get-a-pre-built-image).
